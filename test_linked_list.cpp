#include "LinkedList.hpp"

int main (int argc, char *argv[])
{
    {
        LinkedList<int> a;
        for (int i=1; i<=5; i++) a.append(i);
        std::cout << "a ="; a.print();

        a.reverse(2);
        std::cout << "a.reverse(2)\n" "a ="; a.print();
    }

    {
        LinkedList<int> b;
        for (int i=1; i<=5; i++) b.append(i);
        std::cout << "b ="; b.print();

        b.reverse(3);
        std::cout << "b.reverse(3)\n" "b ="; b.print();
    }

    return 0;
}
