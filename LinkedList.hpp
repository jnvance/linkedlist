#pragma once
#include <iostream>

template<typename T>
struct Node
{
    T value;
    Node* next = nullptr;
};

template<typename T>
struct LinkedList
{
private:
    /// pointer to the first node
    Node<T>* head = nullptr;
    /// pointer to the last node
    Node<T>* tail = nullptr;

    Node<T>* pop_node()
    {
        if (head == nullptr)
        {
            return nullptr;
        }
        else
        {
            Node<T>* old = head;
            head = head->next;
            if (head == nullptr) tail = nullptr;
            old->next = nullptr;
            return old;
        }
    }

    /// Pop the first k nodes into a new LinkedList. If LinkedList is too short, return empty list
    LinkedList pop_nodes(size_t k)
    {
        LinkedList ll;
        if (!is_empty())
        {
            Node<T>* node = head;
            size_t i=1;
            while(i < k && node->next != nullptr)
            {
                node = node->next;
                i++;
            }
            if (i == k)
            {
                ll.head = head;
                head = node->next;
                ll.tail = node;
                ll.tail->next = nullptr;
            }
        }
        return ll;
    }

    /// Add a new node at the beginning
    void push_node(Node<T>* new_node)
    {
        assert(new_node->next==nullptr);

        if (head == nullptr)
        {
            head = new_node;
            tail = head;
        }
        else
        {
            new_node->next = head;
            head = new_node;
        }
    }

    /// move ownership of nodes from another LinkedList
    void move(LinkedList & ll)
    {
        assert(is_empty());
        head = ll.head;
        tail = ll.tail;
        ll.head = nullptr;
        ll.tail = nullptr;
    }

public:
    LinkedList(){}

    ~LinkedList()
    {
        clear();
    }

    void clear()
    {
        while(!is_empty())
        {
            delete pop_node();
        }
    }

    /// checks if container is empty
    bool is_empty() const
    {
        return (head == nullptr);
    }

    /// Add a new node at the beginning
    void push(T const& v)
    {
        auto* new_node = new Node<T>;
        new_node->value = v;
        push_node(new_node);
    }

    /// Remove and return node at the beginning
    T pop()
    {
        if (head == nullptr)
        {
            throw std::out_of_range("Nothing to pop. LinkedList is already empty.");
        }
        Node<T>* node = this->pop_node();
        T val = node->value;
        delete node;
        return val;
    }

    /// Add a new node at the end
    void append(T const& v)
    {
        Node<T>* new_node = new Node<T>;
        new_node->value = v;
        if (head == nullptr)
        {
            head = new_node;
            tail = new_node;
        }
        else
        {
            tail->next = new_node;
            tail = new_node;
        }
    }

    /// Append entire input LinkedList at the end and move ownership of nodes
    void append(LinkedList& list)
    {
        if (!list.is_empty())
        {
            if (is_empty())
            {
                move(list);
            }
            else
            {
                tail->next = list.head;
                tail = list.tail;
                list.head = nullptr;
                list.tail = nullptr;
            }
        }
    }

    /// Reverse entire linked-list
    void reverse()
    {
        LinkedList ll;
        while(!is_empty())
        {
            Node<T>* old = pop_node();
            ll.push_node(old);
        }
        move(ll);
    }

    /// Reverse the nodes k at a time, excluding nodes left out if the number of nodes is not a
    /// multiple of k. This is done by popping and reversing k nodes at a time and appending to
    /// a running LinkedList. Remaining elements are appended without reversing.
    void reverse(size_t k)
    {
        LinkedList l;
        while(!is_empty())
        {
            LinkedList ll = pop_nodes(k);
            if (ll.is_empty()) break;
            ll.reverse();
            l.append(ll);
        }
        l.append(*this);
        assert(is_empty());
        move(l);
    }

    void print() const
    {
        Node<T>* node = head;
        while (node != nullptr)
        {
            std::cout << " " << node->value;
            node = node->next;
        }
        std::cout << std::endl;
        std::cout << "head = " << (head!=nullptr ? std::to_string(head->value) : "NULL") << std::endl;
        std::cout << "tail = " << (tail!=nullptr ? std::to_string(tail->value) : "NULL") << std::endl;
        std::cout << std::endl;
    }

};
